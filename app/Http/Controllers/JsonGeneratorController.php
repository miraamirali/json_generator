<?php

namespace App\Http\Controllers;

use App\Imports\DumpFile;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\Storage;

class JsonGeneratorController extends Controller
{
    private $_file_name = 'json_result.json';
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        ini_set('memory_limit', '888M');
        set_time_limit(-1);
        $arr_slice = ($request->slice) ? explode(',', $request->slice) : null;
        $get_all = isset($request->getAll) ? 1 : 0;

        //$path = $request->file('file-uploaded')->path();
        $this->_file_name = explode('.',$request->file('file-uploaded')->getClientOriginalName())[0] . '.json';

         $extension = $request->file('file-uploaded')->extension();
         $extension = (\in_array( $extension, ['ods', 'xlsx'])) ? $extension : 'csv';


        if ($request->hasFile('file-uploaded')) {

            $path = $request->file('file-uploaded')->store('excel-files');
            Storage::delete('excel-files/file.' . $extension);
            Storage::move($path, 'excel-files/file.' . $extension);
            $rows= (new FastExcel)->import(storage_path('app/excel-files/file.' . $extension));

            $rows = $this->getSliceFast($rows, $arr_slice);

            $this->createJsonFile($rows);
            return response()->download($this->_file_name)->deleteFileAfterSend();
        }
    }

    protected function getSliceFast($collection, $arr_slice = [])
    {
        $arr = $collection->toArray();
        $slice = [];
        foreach($arr as $row){
            if($arr_slice){
                $filtered = Arr::only($row, $arr_slice);
                $slice[] = $this->getColumnsWithSort($filtered, $arr_slice);
            }else{
                $slice[] = $row;
            }
        }
        return $slice;
    }
    protected function getColumnsWithSort($row, $arr_slice)
    {
        $filtered = [];
        foreach($arr_slice as $akey){
            $filtered[$akey] = isset($row[$akey]) ? \trim($row[$akey]) : '';
        }
        return $filtered;
    }

    protected function createJsonFile($content)
    {
        $fp = fopen($this->_file_name, 'w');
        fwrite($fp, json_encode($content));
        fclose($fp);
        return true;
    }
    protected function getSlice($content, $arr_slice = [])
    {
        $filtered = Arr::except($content, [0]);
        $array_keys = ($arr_slice) ? array_values(Arr::only($content[0], $arr_slice)) : array_values($content[0]);
        $slice = [];
        foreach ($filtered as $rowKey => $rowValue) {
            $array_values = ($arr_slice) ? array_values(Arr::only($rowValue, $arr_slice)) : array_values($rowValue);
            $slice[] = array_combine($array_keys, $array_values);
        }
        return $slice;
    }
}

