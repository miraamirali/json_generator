<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Json Gerenator</title>
</head>

<body>


    <div class="card">
        <div class="card-body" style="align-self: center;">
        <h1>Json Generator</h1>
            {{ Form::open(['route' => 'upload_file', 'files' => true, 'method' => 'post' ]) }}
            <div class="form-row align-items-center">
                <div class="col-sm-3 my-1">
                    <label class="sr-only" for="inlineFormInputName">Name</label>
                    {{ Form::text('slice','Position,Account Number,IRR,Quantity,Average Cost,Market Price,Purchase Value,Value,Un-realized Gain or Loss,Un-realized Gain or Loss percentage' ,['class' => 'form-control', 'placeholder' => 'Position,Account Number,IRR']) }}
                </div>
                <div class="col-sm-4 custom-file">
                    {{ Form::file('file-uploaded', $attributes = ['class' => 'custom-file-input', 'id' => 'customFile', 'required' => 'required']) }}
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                <div class="col-auto my-1">
                    <div class="form-check">
                        <input name="getAll" class="form-check-input" type="checkbox" id="autoSizingCheck2">
                        <label class="form-check-label" for="autoSizingCheck2">
                            Get with header
                        </label>
                    </div>
                </div>
                <div class="col-auto my-1">
                    {{ Form::submit('Generate Json File!',['class' => 'btn btn-primary']) }}
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>
